//============================================================================
/// \file   DeclarativeInputEngine.cpp
/// \author Uwe Kindler
/// \date   08.01.2015
/// \brief  Implementation of CDeclarativeInputEngine
///
/// Copyright 2015 Uwe Kindler
/// Licensed under MIT see LICENSE.MIT in project root

#include "DeclarativeInputEngine.h"

#include <QGuiApplication>
#include <QInputMethodEvent>
#include <QJSEngine>
#include <QQmlEngine>
#include <QTimer>
#include <QtQml>

struct DeclarativeInputEnginePrivate
{
    QRect keyboardRectangle;
    DeclarativeInputEngine *inputEngine = nullptr;
    QTimer *animatingFinishedTimer = nullptr; //< triggers position adjustment of focused QML item is covered by keybaord rectangle
    DeclarativeInputEngine::InputMode inputMode = DeclarativeInputEngine::InputMode::Latin;
    bool animating = false;

    DeclarativeInputEnginePrivate(DeclarativeInputEngine *inputEngine)
        : inputEngine(inputEngine)
        , inputMode(DeclarativeInputEngine::Latin)
        , animating(false)
    {}
};

DeclarativeInputEngine::DeclarativeInputEngine(QObject *parent)
    : QObject(parent)
    , d(new DeclarativeInputEnginePrivate(this))
{
    d->animatingFinishedTimer = new QTimer(this);
    d->animatingFinishedTimer->setSingleShot(true);
    d->animatingFinishedTimer->setInterval(100);
    connect(d->animatingFinishedTimer, &QTimer::timeout, this, &DeclarativeInputEngine::animatingFinished);
}

DeclarativeInputEngine::~DeclarativeInputEngine()
{
    delete d;
}

void DeclarativeInputEngine::virtualKeyCancel() {}

bool DeclarativeInputEngine::virtualKeyClick(Qt::Key key, const QString &text, Qt::KeyboardModifiers modifiers)
{
    Q_UNUSED(key)
    Q_UNUSED(modifiers)

    QInputMethodEvent ev;
    if (text == QString("\x7F")) {
        //delete one char
        ev.setCommitString("", -1, 1);

    } else {
        //add some text
        ev.setCommitString(text);
    }
    QGuiApplication::sendEvent(QGuiApplication::focusObject(), &ev);
    return true;
}

void DeclarativeInputEngine::sendKeyToFocusItem(const QString &text)
{
    QInputMethodEvent ev;
    if (text == QString("\x7F")) {
        //delete one char
        ev.setCommitString("", -1, 1);

    } else {
        //add some text
        ev.setCommitString(text);
    }
    QGuiApplication::sendEvent(QGuiApplication::focusObject(), &ev);
}

bool DeclarativeInputEngine::virtualKeyPress(Qt::Key key, const QString &text, Qt::KeyboardModifiers modifiers, bool repeat)
{
    Q_UNUSED(key)
    Q_UNUSED(text)
    Q_UNUSED(modifiers)
    Q_UNUSED(repeat)

    // not implemented yet
    return true;
}

bool DeclarativeInputEngine::virtualKeyRelease(Qt::Key key, const QString &text, Qt::KeyboardModifiers modifiers)
{
    Q_UNUSED(key)
    Q_UNUSED(text)
    Q_UNUSED(modifiers)

    // not implemented yet
    return true;
}

QRect DeclarativeInputEngine::keyboardRectangle() const
{
    return d->keyboardRectangle;
}

void DeclarativeInputEngine::setKeyboardRectangle(const QRect &rect)
{
    setAnimating(true);
    d->animatingFinishedTimer->start(100);
    d->keyboardRectangle = rect;
    emit keyboardRectangleChanged();
}

bool DeclarativeInputEngine::isAnimating() const
{
    return d->animating;
}

void DeclarativeInputEngine::setAnimating(bool animating)
{
    if (d->animating != animating) {
        d->animating = animating;
        emit animatingChanged();
    }
}

void DeclarativeInputEngine::animatingFinished()
{
    setAnimating(false);
}

int DeclarativeInputEngine::inputMode() const
{
    return d->inputMode;
}

void DeclarativeInputEngine::setInputMode(int mode)
{
    if (mode != d->inputMode) {
        d->inputMode = static_cast<DeclarativeInputEngine::InputMode>(mode);
        emit inputModeChanged();
    }
}
