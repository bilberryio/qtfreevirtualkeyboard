/// \file   VirtualKeyboardInputContext.cpp
/// \author Uwe Kindler
/// \date   08.01.2015
/// \brief  Implementation of VirtualKeyboardInputContext
///
/// Copyright 2015 Uwe Kindler
/// Licensed under MIT see LICENSE.MIT in project root

#include <QEvent>
#include <QGuiApplication>
#include <QJSEngine>
#include <QPropertyAnimation>
#include <QQmlContext>
#include <QQmlEngine>
#include <QVariant>

#include "DeclarativeInputEngine.h"
#include "VirtualKeyboardInputContext.h"
#include <private/qquickflickable_p.h>

struct VirtualKeyboardInputContextPrivate
{
    QQuickFlickable *flickable = nullptr;
    bool visible = false;
    DeclarativeInputEngine *inputEngine = new DeclarativeInputEngine();
    QPropertyAnimation *flickableContentScrollAnimation = nullptr; //< for smooth scrolling of flickable content item
};

VirtualKeyboardInputContext::VirtualKeyboardInputContext()
    : QPlatformInputContext()
    , d(new VirtualKeyboardInputContextPrivate)
{
    d->flickableContentScrollAnimation = new QPropertyAnimation(this);
    d->flickableContentScrollAnimation->setPropertyName("contentY");
    d->flickableContentScrollAnimation->setDuration(400);
    d->flickableContentScrollAnimation->setEasingCurve(QEasingCurve(QEasingCurve::OutBack));
    qmlRegisterSingletonType<DeclarativeInputEngine>("FreeVirtualKeyboard", 1, 0, "InputEngine", inputEngineProvider);
}

VirtualKeyboardInputContext::~VirtualKeyboardInputContext() {}

VirtualKeyboardInputContext *VirtualKeyboardInputContext::instance()
{
    static VirtualKeyboardInputContext *InputContextInstance = new VirtualKeyboardInputContext;
    return InputContextInstance;
}

bool VirtualKeyboardInputContext::isValid() const
{
    return true;
}

QRectF VirtualKeyboardInputContext::keyboardRect() const
{
    return QRectF();
}

void VirtualKeyboardInputContext::showInputPanel()
{
    d->visible = true;
    QPlatformInputContext::showInputPanel();
    emitInputPanelVisibleChanged();
}

void VirtualKeyboardInputContext::hideInputPanel()
{
    d->visible = false;
    QPlatformInputContext::hideInputPanel();
    emitInputPanelVisibleChanged();
}

bool VirtualKeyboardInputContext::isInputPanelVisible() const
{
    return d->visible;
}

bool VirtualKeyboardInputContext::isAnimating() const
{
    return false;
}

void VirtualKeyboardInputContext::setFocusObject(QObject *object)
{
    static const int numericInputHints = Qt::ImhPreferNumbers | Qt::ImhDate | Qt::ImhTime | Qt::ImhDigitsOnly | Qt::ImhFormattedNumbersOnly;
    static const int dialableInputHints = Qt::ImhDialableCharactersOnly;

    if (!object) {
        return;
    }

    // we only support QML at the moment - so if this is not a QML item, then
    // we leave immediatelly
    QQuickItem *focusItem = dynamic_cast<QQuickItem *>(object);
    if (!focusItem) {
        return;
    }

    // Check if an input control has focus that accepts text input - if not,
    // then we can leave immediatelly
    bool acceptsInput = focusItem->inputMethodQuery(Qt::ImEnabled).toBool();
    if (!acceptsInput) {
        return;
    }

    // Set input mode depending on input method hints queried from focused
    // object / item
    Qt::InputMethodHints inputMethodHints(focusItem->inputMethodQuery(Qt::ImHints).toInt());
    if (inputMethodHints & dialableInputHints) {
        d->inputEngine->setInputMode(DeclarativeInputEngine::Dialable);
    } else if (inputMethodHints & numericInputHints) {
        d->inputEngine->setInputMode(DeclarativeInputEngine::Numeric);
    } else {
        d->inputEngine->setInputMode(DeclarativeInputEngine::Latin);
    }

    // Search for the top most flickable so that we can scroll the control
    // into the visible area, if the keyboard hides the control
    QQuickItem *item = focusItem;
    d->flickable = nullptr;
    while (item) {
        QQuickFlickable *flickable = dynamic_cast<QQuickFlickable *>(item);
        if (flickable) {
            d->flickable = flickable;
        }
        item = item->parentItem();
    }

    ensureFocusedObjectVisible(focusItem);
}

void VirtualKeyboardInputContext::ensureFocusedObjectVisible(QQuickItem *focusItem)
{
    // If the keyboard is hidden, no scrollable element exists or the keyboard
    // is just animating, then we leave here
    if (!d->visible || !d->flickable || !focusItem || d->inputEngine->isAnimating()) {
        return;
    }

    QRectF focusItemRect(0, 0, focusItem->width(), focusItem->height());
    focusItemRect = d->flickable->mapRectFromItem(focusItem, focusItemRect);
    d->flickableContentScrollAnimation->setTargetObject(d->flickable);
    qreal contentY = d->flickable->contentY();
    if (focusItemRect.bottom() >= d->flickable->height()) {
        contentY = d->flickable->contentY() + (focusItemRect.bottom() - d->flickable->height()) + 20;
        d->flickableContentScrollAnimation->setEndValue(contentY);
        d->flickableContentScrollAnimation->start();
    } else if (focusItemRect.top() < 0) {
        contentY = d->flickable->contentY() + focusItemRect.top() - 20;
        d->flickableContentScrollAnimation->setEndValue(contentY);
        d->flickableContentScrollAnimation->start();
    }
}

QObject *VirtualKeyboardInputContext::inputEngineProvider(QQmlEngine *engine, QJSEngine *scriptEngine)
{
    Q_UNUSED(engine)
    Q_UNUSED(scriptEngine)
    return VirtualKeyboardInputContext::instance()->d->inputEngine;
}
